import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';

import { NavbarComponent } from './components/navbar/navbar.component';
import {NgsRevealModule} from 'ng2-scrollreveal';
import { environment } from './../environments/environment';



const  config = {
  apiKey: "AIzaSyDokJV749l0feuuie1hGTWiFSvy3LFRGrk",
  authDomain: "angular-firebase-98780.firebaseapp.com",
  databaseURL: "https://angular-firebase-98780.firebaseio.com",
  projectId: "angular-firebase-98780",
  storageBucket: "angular-firebase-98780.appspot.com",
  messagingSenderId: "989298693214"
};
const appRoutes: Routes =[
  {path: '', component: HomeComponent},
  
];
@NgModule({

  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    NgsRevealModule.forRoot()

  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [RouterModule]
})
export class AppModule { }

import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { DOCUMENT} from '@angular/platform-browser';
import { Http } from '@angular/http';
import {   trigger, state, animate, transition, style } from '@angular/animations';
import { KalendarzService } from '../../services/kalendarz.service';
import { FirebaseService } from '../../services/firebase.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';
import { PokaService } from '../../services/poka.service';
import * as firebase from 'firebase/app';
import { wideo } from '../kalendarz/loading';
import $ from "jquery";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  providers: [ KalendarzService ],
  styleUrls: ['./navbar.component.css'],
  animations: [
    trigger('vis',[
      state('large',style({ transform: 'scale(1.0)'})),
      state('small',style({transform: 'scale(0.0)'})),
      transition('small => large', animate('300ms ease-out')),
      transition('large=> small', animate('900ms ease-out' )),
    ])
  ],
  host: {'[@vis]': ''}
})
export class NavbarComponent implements OnInit {
 poka:boolean=true;
  nav: boolean=true;
  hamburger : boolean=false;
  menu: boolean=false;
  stejt: String='small';
  zwrot: boolean;
  navIsFixed: boolean =false;
  bedzie: number=0;
  user : Observable<firebase.User>;
  admin: boolean=false;
  maly: boolean=false;
  constructor(@Inject(DOCUMENT) private document: any, public af: AngularFireAuth, private router: Router,
          public _poka: PokaService,private fs: FirebaseService, private h: Http ) {

  //  this.poka=false;
    this.user=af.authState;
  //  this._poka.home();


   }

  login(){
    this.fs.login();
    console.log('af auth');
    console.log(this.af.authState);
    //this.poka=true;
    this.af.authState.subscribe((data)=>{
      console.log(data);
      if(data!=null){
        if(data.uid==='w4kcczIP6reet1uXjozH80UwA6K2'){
          this.admin=true;
        }else{
          this.admin=false;
        }
      }


    });


  }

  logout(){
    this.fs.logout();
    this.admin=false;
  }
  doTerminow(){
    this.router.navigate(['/listings']);
  }


  @HostListener("window:scroll",[])
  onWindowScroll(){
    let number=this.document.body.scrollTop;
    if(number > 800){
      console.log("ZASZLO");
      this.navIsFixed=true;
      this.stejt='large';
    }else if(this.navIsFixed && number < 10){
      this.navIsFixed = false;
          this.stejt='small';
    }
  }
  ngAfterViewChecked(){
    //console.log("COS TU COS");
   if(this.bedzie < 2){
      wideo(this.bedzie);
      console.log("USTAWIAM ZWROT: "+this.zwrot);
      this.bedzie++;
   }

  }
  ngOnInit() {

    this.poka=false;
    console.log('POKA ADMINA:');
    console.log(this.fs.admin);
      this.admin=this.fs.admin;

      this.fs.af.authState.subscribe((data)=>{
        if(data){
        if(data.uid=='w4kcczIP6reet1uXjozH80UwA6K2'){
          this.admin=true;
        }
      }
    });


    console.log("POKA NAVBAR:");
    console.log(this.poka);
    this.poka=false;

    if(window.screen.width <=660){
      this.nav=false;
      this.hamburger=true;
      this.maly=true;

    }
    this._poka.getPoka().subscribe((lol)=>{
      console.log('Odpowiedz o poka');
      console.log(lol);
      this.poka=lol;


    });

  }

  onResize(event){
  //  console.log("Dziala");
  //  console.log(event);

    console.log(event);
    console.log('Target width: '+event.target.innerWidth);

    let lol :number=event.currentTarget.innerWidth;
    if(lol <=660){
      console.log('ZASZEDL KONSTRUKTOR');
      console.log(this._poka.getHome());
      if(this._poka.getHome()){
        console.log("ZASZEDL OFF");
      this._poka.pokaOff();
      }
      this.nav=false;
      this.hamburger=true;
      this.maly=true;
    }
    if(lol >660){

      this.nav=true;
      console.log("GET HOME");
      console.log(this._poka.getHome())
      if(this._poka.getHome()){
            console.log("ZASZEDL ONN");
      this._poka.pokaOn();
    }
      this.hamburger=false;
      this.menu=false;
      this.maly=false;
    }

  }
  toggleMenu(){
    this.menu=! this.menu;
  }



}
